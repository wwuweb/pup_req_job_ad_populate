# README #
## PUP Req Job Ad Pre-Populate ##

[Jump to Installation Instructions](#markdown-header-to-install)

### Purpose ###

Reduce the duplication of effort in filling out the PageUp People (PUP) Position Description Job Advertisement by dynamically populating the Wysiwyg text area with data from the relevant fields in the form.

### What is it ###

Files: pup_req_job_ad_populate_WWU_edits.js
A JavaScript file that automates populating the textarea with data from adjacent fields

### How it works ###

Note: This is a proof of concept;

When the JavaScript (pup_req_job_ad_populate_WWU_edits.js) is called a job posting template is inserted into the job advertisement text area. This template is populated from relevant field values elsewhere on the page.

The JavaScript source code can be run locally or called from a remote host. No remote resources are called. This script only interacts w/ elements available on the current page.

### Installation Notes ###

You will need to be able to run the JavaScript on the requisition page.
There are several options to do this. For example you could open a console in your browser and paste in the code from: pup_req_job_ad_populate_WWU_edits.js.

Alternatively you could store the JavaScript in a [bookmarklet (wikipedia ref.)](https://en.wikipedia.org/wiki/Bookmarklet).

There are two types of bookmarklets to choose from.
The bookmarklet can refer to the remote source code, or host it locally.
There are advantages to both methods; In short, use the remote source code version if you trust the source and want to use the latest version as it's available. Think: cloud hosted.

Use the locally hosted version if you want control over the source code and you don't anticipate any changes.

The master branch of this repository is geared toward remote hosted source code.

The stand-alone alone branch accommodates a locally hosted bookmarklet but may have few features due to single-line storage limitations.

#### To Install ####
To install this bookmarklet, drag the following link to your bookmark toolbar and replace the source with the source code below.
[PUP Req Job Ad Pre-Populate](replace me)

The source code of the remote hosted bookmarklet is:
`javascript:
void(z=document.body.appendChild(document.createElement('script')));
void(z.language='javascript');
void(z.type='text/javascript');
void(z.src='https://bitbucket.org/_vid/pup_req_job_ad_populate/raw/master/pup_req_job_ad_populate_WWU_edits.js');
`

**Note: Currently in practice: **
In lieu of the old Job Ad. description (Sub Text 2):
` Using the position details above and one of the advertising templates {URL}, create the posting announcement in the field below.`
We added this:
`<a href="#" onclick="javascript:
void(z=document.body.appendChild(document.createElement('script')));
void(z.language='javascript');
void(z.type='text/javascript');
void(z.id='pup-req-pop');
void(z.src='https://hr.uoregon.edu/pup-req-pop'); return false;">Job Ad Pre-Populate</a>
`

Side note: if you would rather link to a specific version of the code you can replace the branch name (master) with the version (tag name) in the src URL.
Ex:
https://bitbucket.org/_vid/pup_req_job_ad_populate/raw/pup_req_job_ad_populate-1.1/pup_req_job_ad_populate_WWU_edits.js

### Usage ###

 * Log in and Authenticate to your PUP account: https://admin.pageuppeople.com
 * Navigate to manage requisitions: https://admin.dc4.pageuppeople.com/v5.3/provider/manageJobs/manageJobs.asp
 * Open a position
 * Click the bookmarklet
 * If there is existing text in the text area you will be prompted to overwrite it
 * Bask in the pre-populated text that will appear in the job advertisement text area

### Road Map ###

 * Deploy this to a remote server for serving code
    * Use a git hook https://www.digitalocean.com/community/tutorials/how-to-use-git-hooks-to-automate-development-and-deployment-tasks
 * Add additional templates to support different job types. Done (134a08f)
 * Add support for getting essential duties out of the table format. Done (bf1d707)

### Current maintainer ###

* Vid Rowan ([_vid](http://drupal.org/user/631512/))
