javascript:
/**
 * This code is served up remotely to ensure quality source control.
 * The local Bookmarklet src:
javascript:
void(z=document.body.appendChild(document.createElement('script')));
void(z.language='javascript');
void(z.type='text/javascript');
void(z.src='https://bitbucket.org/_vid/pup_req_job_ad_populate/raw/feature-templates_by_type/pup_req_job_ad_populate.js');
 * *** Contents ***
 * Functions
 * Object mapping, capturing values of fields on the page: 'pupPdElements'
 * Create template w/ object values inserted
 * Trigger MCE code evaluation (to format code if needed)
 ** - Vid
 */
 /**
* Using a url instead of a bookmarklet we could use the following Text changed for WWU to fit the code within 255 characters:
* <a href="#" onclick="javascript:
* void(z=document.body.appendChild(document.createElement('script')));
* void(z.language='javascript');
* void(z.type='text/javascript');
* void(z.src='https://www.wwu.edu/hrdocs/pageup-ad-text/pup_req_job_ad_populate_WWU_edits.js');">
* Populate Text</a>
This way we would not need to add a local bookmarklet to every user that is in PageUp that creates recruitment requests.
** - Joshua
*/


function getMceFrame(wysiwygIframeId){
  var x = document.getElementById(wysiwygIframeId);
  var y = (x.contentWindow || x.contentDocument);
  if (y.document)y = y.document;
  return y.getElementById('tinymce');
}
function replaceText(text,wysiwygIframeId) {
  var sel;
  sel = getMceFrame(wysiwygIframeId);
  if (sel) {
    if ((sel.innerHTML.length > 30 && confirm('Are you sure you want to replace the current text to appear in job advertisement?')) || sel.innerHTML.length <= 30) {
      sel.innerHTML = text;
    }
  }
}
function getTextField(elementID){
  if (document.getElementById(elementID) !== null) {
    return document.getElementById(elementID).value;
  }
  return '';
}

//Replace with <p> to allow for easier bullet listing in PageUp ad text
function getTextAreaToField(elementID){
  if (document.getElementById(elementID) !== null) {
	return document.getElementById(elementID).value.replace(/(?:\r\n|\r|\n)/g, '<p>');
  }
  return '';
}

function getBooleanField(elementID){
  if (document.getElementById(elementID) !== null) {
    if (document.getElementById(elementID).checked){
      return "Yes";
    } else {
      return "No";
    }
  }
  return '';
}
function getSelectField(elementID){
  if (document.getElementById(elementID) !== null) {
    return document.getElementById(elementID).options[document.getElementById(elementID).selectedIndex].text;
  }
  return '';
}
/* ex getSelectField("lDepartmentID"); */
function getDropSearchField(elementID){
  result = null;
  if (typeof document.querySelectorAll('#' + elementID + ' .result-selected')[0] !== "undefined") {
    result = document.querySelectorAll('#' + elementID + ' .result-selected')[0].innerHTML;
  }
  else
    if (typeof document.querySelectorAll('#' + elementID + ' .chosen-single span')[0] !== "undefined") {
      result = document.querySelectorAll('#' + elementID + ' .chosen-single span')[0].innerHTML;
    }
  return result;
}
/* ex getDropSearchField("GenericListType_appointment_chosen"); */

function getEssentialJobDuties(){
  /* for each document.querySelectorAll('#JobDutyWrapper div.jobDuty')
  if(div.jobDuty .dutyLevel == "Essential")
  div.jobDuty .dutyPercent
  div.jobDuty .dutyDuties
  */
  dutyOutput = [];
  dutyList = document.querySelectorAll('#JobDutyWrapper div.jobDuty');
  for(i=0;i<dutyList.length;i++){
    //console.log('Pass: '+i);
    //console.log("what: " + dutyList[i].querySelectorAll('.dutyLevel')[0].innerHTML);
    //console.log(dutyList[i].querySelectorAll('.dutyLevel').length);
    if(dutyList[i].querySelectorAll('.dutyLevel')[0].innerHTML == "Essential"){
      dutyOutput.push( dutyList[i].querySelectorAll('.dutyPercent')[0].innerHTML + '%' + ' - ' + dutyList[i].querySelectorAll('.dutyDuties')[0].innerHTML);
    }
  }
  return dutyOutput.join("<br />");
}
/**
 * Object mapping, capturing values of fields on the page: 'pupPdElements'
 * Set up the data elements (tokens) to populate the wysiwyg:
 */
var pupPdElements = {
  pupPdClassTitle:    getTextField('sTitle'),
  pupPdDept:          getTextField('lDepartmentID'),
  pupPdJobDesc:       getTextAreaToField('sTAOther1'),
  pupPdMinReq:        getTextAreaToField('sTAOther2'),
  pupPdPrefQual:      getTextAreaToField('sTAOther3'),
  pupPdSpecInstruct:  getTextAreaToField('sTAOther7'),
};




/**
 * HEREDOC
 Using a non-standard method of HEREDOC in JS so we can have 'pretty' templates
 * Ref: http://stackoverflow.com/questions/805107/creating-multiline-strings-in-javascript#comment-49227719
 * Ref: http://jsfiddle.net/orwellophile/hna15vLw/2/
 */
var HEREDOC;
/* Determine which template to use based on 'Position Type': */
switch (getSelectField('lWorkTypeID')){
  default:
    HEREDOC = function EOF(){ /*!<<<EOF
    <table width="100%">
		<tbody>
			<tr>
				<td style="text-align: left;">
					Position Title
				</td>
				<td style="text-align: left;">
					<span class="Job-Title" id="pupPdClassTitle">${pupPdClassTitle}</span></ br>
				</td>
			</tr>
			<tr>
				<td style="text-align: left;">
					About the University
				</td>
				<td style="text-align: left;">
					Western Washington University, with over 15,000 students in seven colleges and the graduate school, is nationally recognized for its educational programs, students and faculty. The campus is located in Bellingham, Washington, a coastal community of 83,000 overlooking Bellingham Bay, the San Juan Islands and the North Cascades Mountain range. The city lies 90 miles north of Seattle and 60 miles south of Vancouver, British Columbia.  Western is the highest-ranking public, master's-granting university in the Pacific Northwest, according to the 2017 U.S. News & World Report rankings.
				</td>
			</tr>
			<tr>
				<td style="text-align: left;">
					About the Department
				</td>
				<td style="text-align: left;">
					The _______ College and the ___________ Department support Western's mission to bring together individuals of diverse backgrounds and perspectives in an inclusive, student-centered university that develops the potential of learners and the well-being of communities.  We encourage applications from women, people of color, people with disabilities, veterans, and other candidates from underrepresented backgrounds and with diverse experiences interested in this opportunity.
				</td>
			</tr>
			<tr>
				<td style="text-align: left;">
					About the Position
				</td>
				<td style="text-align: left;">
					<span class="JobDesc" id="pupPdJobDesc">${pupPdJobDesc}</span><br />
				</td>
			</tr>
			<tr>
				<td style="text-align: left;">
					Required Qualifications
				</td>
				<td style="text-align: left;">
					<span class="MinReq" id="pupPdMinReq">${pupPdMinReq}</span><br />
				</td>
			</tr>
			<tr>
				<td style="text-align: left;">
					Preferred Qualifications
				</td>
				<td style="text-align: left;">
					<span class="Preferred-Qualifications" id="pupPdPrefQual">${pupPdPrefQual}</span><br />
				</td>
			</tr>
			<tr>
				<td style="text-align: left;">
					Salary
				</td>
				<td style="text-align: left;">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td style="text-align: left; ">
					Bargaining Unit
				</td>
				<td style="text-align: left;">
					&nbsp;
					</td>
			</tr>
			<tr>
				<td style="text-align: left;">
					Application Instructions
				</td>
				<td style="text-align: left;">
					<p>
					A cover letter and resume are required and should address your experience related to the position responsibilities and the required and preferred qualifications.
					</p>
					<p>
					Please include the names and contact information of three professional references.
					</p>
					<span class="Special-Instructions-to-Applicants" id="pupPdSpecInstruct">${pupPdSpecInstruct}</span><br />
				</td>
			</tr>
			<tr>
				<td style="text-align: left;">
					Closing Date Notes
				</td>
				<td style="text-align: left;">
					HR will input Closing Dates Notes for the position. Ex: 'Application review begins June 12, 2017; position is open until filled"
				</td>
			</tr>
		</tbody>
	</table>
	<table>
		<tbody>
			<tr>
				<td style="text-align: left;">
					<p>
					Western Washington University (WWU) is an equal opportunity and affirmative action employer committed to assembling a diverse, broadly trained faculty and staff. Women, minorities, people with disabilities and veterans are strongly encouraged to apply. In compliance with applicable laws and in furtherance of its commitment to fostering an environment that welcomes and embraces diversity, WWU does not discriminate on the basis of race, color, creed, religion, national origin, sex (including pregnancy and parenting status), disability, age, veteran status, sexual orientation, gender identity or expression, marital status or genetic information in its programs or activities, including employment, admissions, and educational programs. See WWU&rsquo;s Policy on&nbsp;<a style="color: #0563c1; text-decoration: underline;" href="http://www.wwu.edu/policies/docs/1000%20University%20Administration/POL-U1600.02%20Ensuring%20Equal%20Opportunity%20and%20Prohibiting%20Discrimination%20and%20Retaliation.pdf" target="_blank" rel="noopener noreferrer">Ensuring Equal Opportunity and Prohibiting Discrimination and Retaliation</a>. Inquiries may be directed to the Vice Provost for Equal Opportunity and Employment Diversity, Title IX and ADA Coordinator, Equal Opportunity Office, Western Washington University, Old Main 345 (MS 9021), 516 High Street, Bellingham, WA 98225; 360.650.3307 (voice) or 711 (Washington Relay);&nbsp;<a style="color: #0563c1; text-decoration: underline;" href="mailto:eoo@wwu.edu" target="_blank" rel="noopener noreferrer">eoo@wwu.edu</a>
					</p>
					<p>
					WWU is committed to providing reasonable accommodations to qualified individuals with disabilities upon request. To request this document in an alternate format or to request an accommodation, please contact&nbsp;<a style="color: #0563c1; text-decoration: underline;" href="https://wp.wwu.edu/hr/2015/09/02/i-need-a-workplace-accommodation/" target="_blank" rel="noopener noreferrer">Human Resources Disability Services</a>, 360.650.3774 or 711 (Washington Relay).
					</p>
					<p>
					All new employees must comply with the immunization policy and show employment eligibility verification as required by the U.S. Citizen and Immigration Service before beginning work at WWU. A thorough background check will be conducted on all new hires.
					</p>
				</td>
			</tr>
		</tbody>
	</table>
EOF
*/ }

}

//Had to label the split with EOF to work instead of HEREDOC.name since we have one HEREDOC
adTemplate(HEREDOC.toString().split('EOF')[2]
  /* ES6 style variable interpolation looking only at pupPdElements object. Normally: this[inner]: */
  .replace(/\$\{([^}]+)\}/g,
    function(outer, inner, pos) {
      if (typeof pupPdElements[inner] === "function") {
        return pupPdElements[inner]();
      }
      return pupPdElements[inner];
   }));

function adTemplate(output) {
  replaceText(output, "sOverview_ifr");
};

/*replaceText(classifiedTemplate(), "sOverview_ifr");*/

/**
 * Trigger MCE code evaluation (to format code if needed)
 * click the code button
 * locate the open modal source code div and click() the first button Ok
 */
tinyMCE.activeEditor.buttons.code.onclick();
document.querySelectorAll('[aria-label=\'Source code\'] .mce-panel button')[0].click();